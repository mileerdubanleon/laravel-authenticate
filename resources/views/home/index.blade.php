@extends('layouts.app-master')

@section('content')

<h1>Hola</h1>

    @auth
        <p>Bienvenido <b>{{auth()->user()->name ?? auth()->user()->username}}</b>, estás autenticado a la pagina</p>
        <p>
            <a href="/logout">Cerrar sesión</a>
        </p>
    @endauth

    @guest
        <p>Para ver el contenido <a href="/login">inicia sesión</a></p>
    @endguest
    
@endsection

