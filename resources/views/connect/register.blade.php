@extends('layouts.auth-master')

@section('content')
    <div class="container d-flex align-items-center justify-content-center" style="height: 100vh;">
        <form action="/register" method="POST" class="">
            @csrf
            <h1>Create account</h1>
            @include('layouts.partials.messages')
            <div class="mb-3 form-floating">
                <input type="text" class="form-control" name="name" id="exampleInputEmail1" aria-describedby="emailHelp">
                <label for="exampleInputEmail1" class="form-label">Nombre</label>
            </div>
            <div class="mb-3 form-floating">
                <input type="text" class="form-control" name="username" id="exampleInputEmail1" aria-describedby="emailHelp">
                <label for="exampleInputEmail1" class="form-label">Usuario</label>
            </div>
            <div class="mb-3 form-floating">
                <input type="text" class="form-control" name="email" id="exampleInputEmail1" aria-describedby="emailHelp">
                <label for="exampleInputEmail1" class="form-label">Correo</label>
            </div>
            <div class="mb-3 form-floating">
                <input type="password" class="form-control" name="password" id="exampleInputPassword1">
                <label for="exampleInputPassword1" class="form-label">Contraseña</label>
            </div>
            <div class="mb-3 form-floating">
                <input type="password" class="form-control" name="password_confirmation" id="exampleInputPassword1">
                <label for="exampleInputPassword1" class="form-label">Confirmar contraseña</label>
            </div>
            <div class="my-3">
                <span class="form-text">Inicia sesión <a href="/login">aquí</a></span>
            </div>

            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@endsection